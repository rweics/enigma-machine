package enigma;

/** Class that represents a complete enigma machine.
 *  @author Ran Wei
 */
class Machine {

    /** Set my rotors to (from left to right) ROTORS.  Initially, the rotor
     *  settings are all 'A'. */
    void replaceRotors(Rotor[] r) {
        rotors = r;
    }

    /** Returns the encoding/decoding of MSG, updating the state of
     *  the rotors accordingly. */
    String convert(String msg) {
        char[] converted = new char[msg.length()];
        for (int i = 0; i < msg.length(); i+=1)
            converted[i] = keyPressed(msg.charAt(i));
        String result = new String(converted);
        return result;
    }
    
    /** Return character output from the machine when a key is pressed. */
    char keyPressed(char entry) {
        if (rotors[2].atNotch() && rotors[3].atNotch())
            rotors[2].advance();
        if (rotors[3].atNotch())
            rotors[3].advance();
        rotors[4].advance();
        int letterIndex = Rotor.toIndex(entry);
        for (int i = 4; i >= 0; i-=1)
            letterIndex = rotors[i].convertForward(letterIndex);
        for (int i = 1; i < 5; i+=1)
            letterIndex = rotors[i].convertBackward(letterIndex);
        char output = Rotor.toLetter(letterIndex);
        return output;
    }
    
    /** Rotors contained in the machine. */
    public Rotor[] rotors = null;

}
