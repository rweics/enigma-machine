package enigma;

/** Class that represents a rotor that has no ratchet and does not advance.
 *  @author Ran Wei
 */
class FixedRotor extends Rotor {

    /** Constructor extended from Rotor class. */
    FixedRotor(String type) {
        super(type);
    }

    /** FixedRotors never advance. */
    @Override
    boolean advances() {
        return false;
    }

    /** FixedRotors don't have notches. */
    @Override
    boolean atNotch() {
        return false;
    }

    /** Fixed rotors do not advance. */
    @Override
    void advance() {
    }

}
