package enigma;

/** Class that represents a rotor in the enigma machine.
 *  @author Ran Wei
 */
class Rotor {

    /** Size of alphabet used for plaintext and ciphertext. */
    static final int ALPHABET_SIZE = 26;

    /** Default constructor, by default use I type rotor. */
    Rotor() {
        specifyType("I");
    }

    /** Constructor that allows user to specify type of rotor when declared. */
    Rotor(String type) {
        specifyType(type);
    }

    /** Assuming that P is an integer in the range 0..25, returns the
     *  corresponding upper-case letter in the range A..Z. */
    static char toLetter(int p) {
        return (char)('A'+p);
    }

    /** Assuming that C is an upper-case letter in the range A-Z, return the
     *  corresponding index in the range 0..25. Inverse of toLetter. */
    static int toIndex(char c) {
        return (int)(c-'A');
    }

    /** Returns true iff this rotor has a ratchet and can advance. */
    boolean advances() {
        return rotorIndex() >= 2 && rotorIndex() <= 4;
    }

    /** Returns true iff this rotor has a left-to-right inverse. */
    boolean hasInverse() {
        return left != null && right != null;
    }

    /** Return my current rotational setting as an integer between 0
     *  and 25 (corresponding to letters 'A' to 'Z').  */
    int getSetting() {
        return _setting;
    }

    /** Set getSetting() to POSN.  */
    void set(int posn) {
        assert 0 <= posn && posn < ALPHABET_SIZE;
        _setting = posn;
    }

    /** Set RotorType to type.  */
    void specifyType(String type) {
        set(0);
        for (int i = 0; i < PermutationData.ROTOR_SPECS.length; i+=1) {
            if (PermutationData.ROTOR_SPECS[i][0].equals(type))
                _SPECS = PermutationData.ROTOR_SPECS[i];
        }
        assert _SPECS != null;
    }

    /** Return the conversion of P (an integer in the range 0..25)
     *  according to my permutation. */
    int convertForward(int p) {
        int in = (p + getSetting() + 26) % 26;
        int converted = (int)(_SPECS[1].charAt(in))-'A';
        int out = (converted - getSetting() + 26) % 26;
        return out;
    }

    /** Return the conversion of E (an integer in the range 0..25)
     *  according to the inverse of my permutation. */
    int convertBackward(int e) {
        int in = (e + getSetting() + 26) % 26;
        int converted = (int)_SPECS[2].charAt(in)-'A';
        int out = (converted - getSetting() + 26) % 26;
        return out;
    }

    /** Return the index(position) of the rotor in a linked chain. */
    int rotorIndex() {
        if (left == null)
            return 0;
        else return 1 + left.rotorIndex();
    }

    /** Returns true iff I am positioned to allow the rotor to my left
     *  to advance. */
    boolean atNotch() {
        for (int i = 0; i < _SPECS[3].length(); i+=1) {
            if (_SPECS[3].charAt(i) == Rotor.toLetter(_setting))
                return true;
        }
        return false;
    }

    /** Advance me one position. */
    void advance() {
        if (atNotch() && left.advances())
            left.advance();
        set((getSetting()+27) % 26);
    }

    /** Pointers to the left and right rotor, null if no rotor. */
    public Rotor left = null;
    public Rotor right = null;

    /** Rotor specs (type should match data in PermutationData). */
    private String[] _SPECS = null;

    /** My current setting (index 0..25, with 0 indicating that 'A'
     *  is showing). */
    private int _setting;

}
