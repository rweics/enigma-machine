package enigma;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/** Enigma simulator.
 *  @author Ran Wei
 */
public final class Main {

    /** Process a sequence of encryptions and decryptions, as
     *  specified in the input from the standard input.  Print the
     *  results on the standard output. Exits normally if there are
     *  no errors in the input; otherwise with code 1. */
    public static void main(String[] unused) {
        Machine M;
        BufferedReader input =
            new BufferedReader(new InputStreamReader(System.in));

        M = null;

        try {
            while (true) {
                String line = input.readLine();
                if (line == null) {
                    System.out.println("ENDED");
                    break;
                }
                if (isConfigurationLine(line)) {
                    M = new Machine();
                    configure(M, line);
                } else {
                    printMessageLine(M.convert(standardize(line)));
                }
            }
        } catch (IOException excp) {
            System.err.printf("Input error: %s%n", excp.getMessage());
            System.exit(1);
        }
    }

    /** Return true iff LINE is an Enigma configuration line. */
    private static boolean isConfigurationLine(String line) {
        String[] configuration = line.toUpperCase().split(" ");
        if (configuration.length != 7)
            return false;
        if (configuration[0].charAt(0) != '*')
            return false;
        if (configuration[1].charAt(0) != 'B' &&
            configuration[1].charAt(0) != 'C')
            return false;
        if (!configuration[2].equals("BETA") &&
            !configuration[2].equals("GAMMA"))
            return false;
        if (configuration[6].length() != 4)
            return false;
        for (int i = 0; i < 4; i += 1) {
            if (!Character.isLetter(configuration[6].charAt(i)))
                return false;
        }
        for (int i = 3; i < 6; i += 1) {
            boolean validRotor = false;
            boolean duplicate = false;
            for (int j = 0; j < PermutationData.ROTOR_SPECS.length; j += 1) {
                if (configuration[i].equals(PermutationData.ROTOR_SPECS[j][0]))
                    validRotor = true;
            }
            for (int j = i+1; j < 6; j += 1) {
                if (configuration[i].equals(configuration[j]))
                    duplicate = true;                
            }
            if (!validRotor || duplicate)
                return false;
        }
        return true;
    }

    /** Configure M according to the specification given on CONFIG,
     *  which must have the format specified in the assignment. */
    public static void configure(Machine M, String config) {
        String[] configuration = config.toUpperCase().split(" ");
        Rotor[] rotors = new Rotor[5];
        rotors[0] = new Reflector(configuration[1]);
        rotors[1] = new FixedRotor(configuration[2]);
        for (int i = 2; i < 5; i+=1)
            rotors[i] = new Rotor(configuration[i+1]);
        for (int i = 0; i < rotors.length-1; i+=1)
            rotors[i].right = rotors[i+1];
        for (int i = 1; i < rotors.length; i+=1) {
            rotors[i].left = rotors[i-1];
            rotors[i].set(Rotor.toIndex(configuration[6].charAt(i-1)));
        }
        M.replaceRotors(rotors);
    }

    /** Return the result of converting LINE to all upper case,
     *  removing all blanks and tabs.  It is an error if LINE contains
     *  characters other than letters and blanks. */
    private static String standardize(String line) throws IOException {
        line = line.toUpperCase().replaceAll(" ","");
        for (int i = 0; i < line.length(); i+=1) {
            if (!Character.isLetter(line.charAt(i)))
                throw new IOException("Invalid character: "+line.charAt(i));
        }
        return line;
    }

    /** Print MSG in groups of five (except that the last group may
     *  have fewer letters). */
    private static void printMessageLine(String msg) {
        int count = 0;
        for (int i = 0; i < msg.length(); i+=1) {
            count += 1;
            System.out.print(msg.charAt(i));
            if (count == 5) {
                System.out.print(' ');
                count = 0;
            }
        }
        System.out.println();
    }
    
}

