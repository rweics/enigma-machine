package enigma;

/** Class that represents a reflector in the enigma.
 *  @author Ran Wei
 */
class Reflector extends Rotor {

    /** Constructor that extends from Rotor class. */
    Reflector(String type) {
        super(type);
    }

    /** Reflectors don't have inverses. */
    @Override
    boolean hasInverse() {
        return false;
    }

    /** Returns a useless value; should never be called. */
    @Override
    int convertBackward(int unused) {
        throw new UnsupportedOperationException();
    }

}